import getMember from './get_member'

export default (userId, callback) => {
  console.log(getMember(userId))
  getMember(userId)
    .then(data => {
      sessionStorage.setItem("uilgo_user_id", userId)
      callback(data)
    })
    .catch(err => {
      alert("Erro ao fazer login", err)
    })
}