import axios from 'axios'

export default userId => new Promise((resolve, reject) => {
  axios(`${process.env.REACT_APP_API_BASE}/members/${userId}`)
    .then(response => {
      resolve(response.data)
    })
    .catch(err => {
      reject(err)
    })
})