import React, { useState, useContext, useEffect } from "react";
import Header from "../layout/header_main";
import Footer from "../layout/footer_main";

import CustomRadio from "../components/form/custom_radio";
import FormTitle from "../components/title_form";
import InputText from "../components/form/input_text";
import UilgoButton from "../components/form/button";

import login from "../functions/login";
import history from "../functions/history";
import axios from "axios";
import { AppContext } from "../App";
import { parseISO, format } from "date-fns";

import { useFormik } from "formik";
import * as Yup from "yup";

const months_pt_br = [
  "Janeiro",
  "Fevereiro",
  "Março",
  "Abril",
  "Maio",
  "Junho",
  "Julho",
  "Agosto",
  "Setembro",
  "Outubro",
  "Novembro",
  "Dezembro",
];

const DateInput = ({ verifyId }) => {
  const [day, setDay] = useState(null);
  const [month, setMonth] = useState(null);
  const [year, setYear] = useState(null);

  return (
    <div className="date_big_input">
      <input
        name="day"
        type="number"
        min="1"
        max="31"
        step="1"
        minLength="2"
        maxLength="2"
        autoFocus={true}
        placeholder="01"
        value={day || ""}
        onChange={(e) => setDay(e.target.value)}
      />{" "}
      /{" "}
      <input
        name="month"
        type="number"
        min="1"
        max="12"
        minLength="2"
        maxLength="2"
        placeholder="12"
        value={month || ""}
        onChange={(e) => setMonth(e.target.value)}
      />{" "}
      /{" "}
      <input
        name="year"
        type="number"
        minLength="4"
        maxLength="4"
        placeholder="1995"
        value={year || ""}
        onChange={(e) => setYear(e.target.value)}
      />
      <UilgoButton
        onClick={(e) => {
          verifyId(`${year}-${month}-${day}`);
        }}
      >
        Verificar
      </UilgoButton>
    </div>
  );
};

const validationSchema = Yup.object().shape({
  name: Yup.string().required("Campo obrigatório!"),
  email: Yup.string()
    .email("Deve ser um email válido")
    .required("Campo obrigatório!"),
  phoneNumber: Yup.string().required("Campo obrigatório!"),
  identityType: Yup.string().required("Campo obrigatório!"),
  identityNumber: Yup.string().required("Campo obrigatório!"),
  sex: Yup.string().required("Campo obrigatório!"),
  birth: Yup.date('Deve ser uma data válida').required("Campo obrigatório!"),
  address: Yup.string().required("Campo obrigatório!"),
  fathersName: Yup.string().required("Campo obrigatório!"),
  fathersPhone: Yup.string().required("Campo obrigatório!"),
  mothersName: Yup.string().required("Campo obrigatório!"),
  mothersPhone: Yup.string().required("Campo obrigatório!"),
  pastorsName: Yup.string().required("Campo obrigatório!"),
  pastorsPhone: Yup.string().required("Campo obrigatório!"),
  tickets: Yup.string().required("Campo obrigatório!"),
});

export default (props) => {
  const context = useContext(AppContext);
  const eventId = props.match.params.id;
  const [tickets, setTickets] = useState(null);
  const [foundUsers, setFoundUsers] = useState(null);
  const [filledWithUser, setFilledWithUser] = useState(null);
  const [eventData, setEventData] = useState(false);

  /** Carrega a descrição e informações do evento */
  const loadDescription = () => {
    axios(
      `${process.env.REACT_APP_API_BASE}/events/${eventId}`
    ).then((response) => setEventData(response.data));
  };

  /** Carrega tickets pelo id do evento */
  const loadTickets = () => {
    axios(`${process.env.REACT_APP_API_BASE}/events/tickets/${eventId}`).then(
      (response) => {
        console.log("loading tickets", response.data);
        setTickets(response.data);
      }
    );
  };

  /** Apenas carregando membros para dar log */
  const loadMembers = () => {
    axios(`${process.env.REACT_APP_API_BASE}/members`).then((response) => {
      response.data.forEach((data) => {
        const dateBirth = new Date(data.birth);
        const strBirth = format(dateBirth, "yyyy'-'MM'-'dd");
      });
    });
  };

  /** Aciona as funções de carregamento de dados */
  useEffect(() => {
    loadDescription();
    loadTickets();
    // loadMembers();
  }, []);

  /**
   * É uma função a ser acionada enquando se tecla no campo de
   * email, nome ou RG, isso serve para verificar se a entrada
   * está compatível com algum registro de membro de banco.
   */
  const checkUser = (e) => {
    const value = e.target.value;
    let foundUsers = [];
    axios(`${process.env.REACT_APP_API_BASE}/members/public/${value}`)
      .then((response) => {
        response.data.map((user) => {
          if (
            user.email === value ||
            user.name === value ||
            user.identityNumber === value
          ) {
            foundUsers.push({
              id: user.id,
            });
          }
        });
        setFoundUsers(foundUsers);
      })
      .catch((err) => console.log("Error", err));
  };

  useEffect(() => {
    if (foundUsers) {
      openModal();
    }
  }, [foundUsers]);

  /** Função disparada ao clicar em VERIFICAR para acionar checkBirth */
  const verifyId = (date) => {
    const parsedBirth = parseISO(date);
    const strBirth = format(parsedBirth, "yyyy'-'MM'-'dd");
    checkBirth(foundUsers[0].id, strBirth);
  };

  /**
   * Verifica se a data de nascimento passada YY-MM-DD
   * é pertencente ao id passado para confimação, retornando
   * um callback em caso de positivo e um alerta em caso
   * negativo
   */
  const checkBirth = (id, date) => {
    console.log(id, date);
    axios(
      `${process.env.REACT_APP_API_BASE}/members/validation/birth?birthDate=${date}&id=${id}`
    )
      .then((response) => {
        console.log("o email confere, id: ", response.data.id);
        setFilledWithUser(response.data);
      })
      .catch((error) => {
        if (error.response) {
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
          if (error.response.status === 404) {
            alert(
              "A data de nascimento não corresponde a nenhum dos usuários cadastrados com esse email. Por favor prossiga cadastrando os dados manualmente."
            );
          }
        }
        console.log("Error:", error);
      });
  };

  useEffect(() => {
    if (filledWithUser) {
      // TODO setValues
      fillForm(filledWithUser.id);
      if (window.confirm("Deseja entrar na UILGO com a conta desse email?")) {
        login(filledWithUser.id, (member) => context.setMember(member));
      }
      context.closeModal();
    }
  }, [filledWithUser]);

  const fillForm = (userId) => {
    axios(`${process.env.REACT_APP_API_BASE}/members/${userId}`)
      .then((response) => {
        let {
          id,
          name,
          email,
          phoneNumber,
          identityNumber,
          identityType,
          sex,
          birth,
          address,
          fathersName,
          fathersPhone,
          mothersName,
          mothersPhone,
          pastorsName,
          pastorsPhone,
        } = response.data;

        formik.setValues({
          ...formik.values,
          userId,
          name,
          email,
          phoneNumber,
          identityNumber,
          identityType,
          sex,
          birth,
          address,
          fathersName,
          fathersPhone,
          mothersName,
          mothersPhone,
          pastorsName,
          pastorsPhone,
        });
      })
      .catch((err) => {
        console.log("Não encontrei o usuário no banco de dados", err);
      });
  };

  /** Função que abre o modal com o conteúdo do formulário de confirmação de data */
  const openModal = (force = false) => {
    if ((foundUsers && foundUsers.length) || force) {
      context.changeModalComponent(
        <div>
          <p>
            Seus dados foram encontrados em nosso banco de dados, confirme sua
            data de nascimento para que possamos preencher seu formulário de
            forma automática
          </p>
          <DateInput verifyId={verifyId} />
        </div>
      );
      context.openModal();
    }
  };

  const formik = useFormik({
    initialValues: {
      userId: null,
    },
    validationSchema,
  });

  // Start date of an event
  const startDate = new Date(eventData.startDate),
    d_ST = startDate.getDate(),
    m_ST = months_pt_br[startDate.getMonth() + 1],
    y_ST = startDate.getFullYear();

  const stopDate = new Date(eventData.stopDate),
    d_SpT = stopDate.getDate(),
    m_SpT = months_pt_br[stopDate.getMonth() + 1],
    y_SpT = stopDate.getFullYear();

  return (
    <div>
      <Header title={eventData.name} />
      <main className="container main_content">
        <h2>Descrição</h2>
        <div className="description">
          <span>
            <strong>Data de início do evento:</strong>{" "}
            {`${d_ST} de ${m_ST} de ${y_ST}`}
          </span>
          <span>
            <strong>Data de término:</strong>{" "}
            {`${d_SpT} de ${m_SpT} de ${y_SpT}`}
          </span>
        </div>

        {/* Primeiro passo: Dados de identificação pessoal */}
        <FormTitle step={1}>Identifique-se</FormTitle>
        <input
          type="hidden"
          label="user_id"
          name="userId"
          id="userId"
          onChange={formik.handleChange}
          value={formik.values.userId}
        />
        {/* Campo interligente de email */}
        <fieldset>
          <legend>Dados pessoais</legend>
          <div>
            <InputText
              label="Email"
              placeholder="johndoe@email.com"
              name="email"
              type="email"
              value={formik.values.email}
              onKeyUp={checkUser}
              onBlur={checkUser}
              formikProps={formik}
              className={foundUsers && foundUsers.length && "green"}
            />
            {foundUsers && foundUsers.length && !filledWithUser && (
              <p>
                Um usuário foi encontrado com esses dados,{" "}
                <button onClick={(e) => openModal()}>
                  clique aqui para completar os dados
                </button>{" "}
                de forma automática
              </p>
            )}
            {/* Campo de nome */}
            <InputText
              label="Nome"
              name="name"
              formikProps={formik}
            />
            {/* Campo de telefone */}
            <InputText
              label="Número de telefone"
              placeholder="+5571988887777"
              name="phoneNumber"
              formikProps={formik}
            />
            {/* Número de identidade */}
            <select
              name="identityType"
              id="identityType"
              onChange={formik.handleChange}
            >
              <option value="">Selecione um documento de identificação</option>
              {["CPF", "RG"].map((type) => (
                <option
                  value={type}
                  selected={formik.values.identityType === type}
                >
                  {type}
                </option>
              ))}
            </select>
            {/* Número de identidade */}
            <InputText
              label={`Número do(a) ${formik.values.identityType}`}
              name="identityNumber"
              formikProps={formik}
            />
            {/* Sexo */}
            <div className="input_wrap">
              <input
                type="radio"
                name="sex"
                id="sex"
                checked={formik.values.sex === "M"}
                value="M"
                onChange={formik.handleChange}
              />{" "}
              Masculino
            </div>
            <div className="input_wrap">
              <input
                type="radio"
                name="sex"
                id="sex"
                checked={formik.values.sex === "F"}
                value="F"
                onChange={formik.handleChange}
              />{" "}
              Feminino
            </div>
            {/* Data de nascimento */}
            <InputText
              type="date"
              label="Data de nascimento"
              name="birth"
              formikProps={formik}
            />
            {/* Endereço */}
            <InputText
              label="Endereço"
              name="address"
              formikProps={formik}
            />
          </div>
        </fieldset>
        <fieldset>
          <legend>Dados de responsáveis</legend>
          {/* Campo do nome e telefone do pai */}
          <div>
            <InputText
              label="Nome do pai"
              name="fathersName"
              formikProps={formik}
            />
            <InputText
              label="Telefone do pai"
              placeholder="+5571988887777"
              name="fathersPhone"
              formikProps={formik}
            />
          </div>
          {/* Campo do nome e telefone da mãe */}
          <div>
            <InputText
              label="Nome da mãe"
              name="mothersName"
              formikProps={formik}
            />
            <InputText
              label="Telefone da mãe"
              placeholder="+5571988887777"
              name="mothersPhone"
              formikProps={formik}
            />
          </div>
          {/* Campo do nome e telefone do pastor */}
          <div>
            <InputText
              label="Nome do pastor"
              name="pastorsName"
              formikProps={formik}
            />
            <InputText
              label="Telefone do pastor"
              placeholder="+5571988887777"
              name="pastorsPhone"
              formikProps={formik}
            />
          </div>
        </fieldset>
        <FormTitle step={2}>Selecione um modo de acomodação</FormTitle>
        {/* <pre>{JSON.stringify(this.state.tickets, null, 2)}</pre> */}
        <div className="tickets flex">
          {tickets &&
            tickets.map((elem) => (
              <CustomRadio
                name="tickets"
                elem={elem}
                className="tickets_radio"
                onChange={formik.handleChange}
              />
            ))}
        </div>
        <UilgoButton
          disabled={!(formik.isValid === true && formik.dirty === true)}
          onClick={(e) => {
            const checkout = ({ ticketId, memberId }) => {
              axios
                .post(
                  `${process.env.REACT_APP_API_BASE}/subscriptions/checkout/${ticketId}/${memberId}`
                )
                .then((response) => {
                  history.push(`/checkout/${response.data.sessionId}`);
                })
                .catch((err) => {
                  console.log(err);
                  alert(
                    "Ocorreu algum erro ao proceder para o pagamento. Por favor verifique seus dados e tente novamente!"
                  );
                });
            };

            if (formik.values.userId) {
              // procede para checkout
              checkout({
                ticketId: formik.values.tickets,
                memberId: formik.values.userId,
              });
            } else {
              const {
                name,
                email,
                phoneNumber,
                identityNumber,
                identityType,
                sex,
                birth,
                address,
                fathersName,
                fathersPhone,
                mothersName,
                mothersPhone,
                pastorsName,
                pastorsPhone,
              } = formik.values;
              const requestData = {
                name,
                email,
                phoneNumber,
                identityNumber,
                identityType,
                sex,
                birth,
                address,
                fathersName,
                fathersPhone,
                mothersName,
                mothersPhone,
                pastorsName,
                pastorsPhone,
              };
              console.log("criando novo membro", requestData);
              axios
                .post(`${process.env.REACT_APP_API_BASE}/members`, requestData)
                .then((response) => {
                  console.log(response);
                  const memberId = response.data;
                  checkout({
                    ticketId: formik.values.tickets,
                    memberId,
                  });
                })
                .catch((err) => {
                  console.log(err);
                  if (err.response) {
                    if (err.response.status === 409) {
                      console.log("email já existe");
                      alert(
                        "Email já existe com outros dados, por favor preencha novamente o seu email e confirme a data de nascimento com o modal que irá aparecer"
                      );
                    }
                  }
                  alert(
                    "Algum erro ocorreu na criação de um novo usuário, verifique se todos os dados foram preenchidos corretamente e tente novamente"
                  );
                });
            }
          }}
        >
          Prosseguir
        </UilgoButton>
        {/* <pre>{JSON.stringify(formik, null, 2)}</pre> */}
      </main>
      <Footer />
    </div>
  );
};
