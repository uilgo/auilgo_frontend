import React from "react";
import Header from "../layout/header_main";
import Footer from "../layout/footer_main";
import { Link } from "react-router-dom";

export default (props) => {
  return (
    <div>
      <Header title="Pagamento Efetuado com sucesso!" />
      <main className="container main_content profile">
        <p>
          Você pode continuar <Link to="/">navegando no site</Link> e fazer
          outras inscrições
        </p>
      </main>
      <Footer />
    </div>
  );
};
