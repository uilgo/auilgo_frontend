import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import Header from '../layout/header_main'
import Footer from '../layout/footer_main'
import UilgoButton from '../components/form/button'
import axios from 'axios'

const months_pt_br = [
  "Janeiro",
  "Fevereiro",
  "Março",
  "Abril",
  "Maio",
  "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];

export default class SimplePage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      events: []
    }
  }

  load_events = () => {
    axios(`${process.env.REACT_APP_API_BASE}/events`)
      .then(response => this.setState({ events: response.data }))
  }

  open_register = (e) => {
    // this.setState({ eventsGeneralClass: "__inactive" });
    console.log(e.target.classList);
  }

  componentDidMount() {
    this.load_events();
  }

  render() {
    return (
      <>
        <Header title="Eventos" />
        <main className="container main_content">
          {this.state.events.map(elem => {

            // Start date of an event
            const startDate = new Date(elem.startDate),
              d_ST = startDate.getDate(),
              m_ST = months_pt_br[startDate.getMonth() + 1],
              y_ST = startDate.getFullYear();

            const stopDate = new Date(elem.stopDate),
              d_SpT = stopDate.getDate(),
              m_SpT = months_pt_br[stopDate.getMonth() + 1],
              y_SpT = stopDate.getFullYear();

            return (
              <section className={`event_box ${this.state.eventsGeneralClass}`} onClick={this.open_event_register_box}>
                <header><h3>{elem.name}</h3></header>
                <div className="date">
                  <span className="beginning">{`${d_ST} de ${m_ST} de ${y_ST}`}</span>
                  <span className="end">{`à ${d_SpT} de ${m_SpT} de ${y_SpT}`}</span>
                  <div className="register">
                    <Link to={`/evento/${elem.id}`}><UilgoButton>Quero participar!</UilgoButton></Link>
                  </div>
                </div>
              </section>
            )
          })}
        </main>
        <Footer />
      </>
    )
  }
}
