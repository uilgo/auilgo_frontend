import React from "react";
import Header from "../layout/header_main";
import Footer from "../layout/footer_main";

export default (props) => {
  return (
    <div>
      <Header title="Pagamento cancelado" />
      <main className="container main_content profile">
        <h2>Você cancelou sua operação de pagamento!</h2>
        <p>Pode continuar navegando no site ou recadastrar na página de eventos.</p>
      </main>
      <Footer />
    </div>
  );
};
