import React, { useContext, useState, useEffect } from "react";
import Header from "../layout/header_main";
import Footer from "../layout/footer_main";
import { AppContext } from "../App";
import getMember from "../functions/get_member";
import InputText from "../components/form/input_text";
import axios from "axios";
import login from "../functions/login";
import { parseISO, format } from "date-fns";
import history from "../functions/history";
import { useFormik } from "formik";


export default () => {
  const context = useContext(AppContext);

  console.log("context", context);

  const userId = sessionStorage.getItem("uilgo_user_id");

  const [name, setName] = useState(null);
  const [memberData, setMemberData] = useState(null);
  const [myEvents, setMyEvents] = useState(null);

  useEffect(() => {
    // Verifica primeiramente se existe registo de login para setar email
    if (context.member.id) {
      setMemberData(context.member);
      setName(context.member.name);
      formik.setFieldValue('email', context.member.email);
      const date = new Date(context.member.birth);
      let strDate = `${date.getDate() + 1}/${
        date.getMonth() + 1
      }/${date.getFullYear()}`;
      formik.setFieldValue('birth', strDate);
    } else if (userId) {
      getMember(userId).then((member) => {
        formik.setFieldValue('email', member.email);
        setName(member.name);
      });
    }
  }, []);

  const validate = userId => {
    const date = new Date(formik.values.birth);
    let strDate = `${date.getFullYear()}-${date.getMonth() + 1}-${
      date.getDate() + 1
    }`;

    axios(
      `${process.env.REACT_APP_API_BASE}/members/validation/birth?birthDate=${strDate}&id=${userId}`
    )
      .then((response) => {
        getMember(response.data.id)
          .then((member) => {
            setMemberData(member);
            getMyEvents(member.id);
            login(member.id, (member) => context.setMember(member));
          })
          .catch((err) => console.log("Erro ao procurar membro", err));
      })
      .catch((error) => {
        if (error.response) {
          if (error.response.status === 404) {
            alert(
              "A data de nascimento não corresponde a nenhum dos usuários cadastrados com esse email. Por favor prossiga cadastrando os dados manualmente."
            );
          }
        }
        console.log("Error:", error);
      });
  }

  const check = (e) => {
    if(userId){
      validate(userId)
    }else{
      axios(`${process.env.REACT_APP_API_BASE}/members/public/${formik.values.email}`)
      .then(response => {
        console.log(response.data)
        if(response.data[0] && response.data[0].id){
          login(response.data[0].id, member => context.setMember(member))
          validate(response.data[0].id)
        }else{
          alert('Não foram encontrados usuários com esses dados')  
        }
      })
      .catch(err => {
        alert('Não foram encontrados usuários com esses dados')
      })
    }
  };

  const getMyEvents = (memberId) => {
    axios(`${process.env.REACT_APP_API_BASE}/subscriptions/member/${memberId}`)
      .then((response) => {
        setMyEvents(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const formik = useFormik({
    initialValues: {}
  })

  return (
    <div>
      <Header title="Perfil" />
      <main className="container main_content profile">
        {name && <h2>Olá {name}</h2>}
        {name && !memberData ? (
          <p>
            Para sua segurança é necessário confirmar os dados abaixo para que
            seus dados pessoais sejam visualizados
          </p>
        ) : null}
        {memberData ? (
          <section>
            <section>
              <h3>Dados pessoais</h3>
              <strong>Email: </strong>
              {memberData.email}
              <br />
              <strong>Telefone: </strong>
              {memberData.phoneNumber}
              <br />
              <strong>{memberData.identityType}: </strong>
              {memberData.identityNumber}
              <br />
              <strong>Data de nascimento: </strong>
              {formik.values.birth}
              <br />
              {/* <strong>Idade: </strong>{memberData.age}<br/> */}
              <strong>Sexo: </strong>
              {memberData.sex}
              <br />
            </section>
            <section>
              <h3>Eventos Cadastrados</h3>
              {myEvents &&
                myEvents.map((subscription) => {
                  console.log('subscription', subscription)
                  const startDate = new Date(
                    subscription.ticket.event.startDate
                  );
                  const strStart = format(startDate, "dd'/'MM'/'yyyy");
                  const stopDate = new Date(subscription.ticket.event.stopDate);
                  const strStop = format(stopDate, "dd'/'MM'/'yyyy");

                  const checkout = ({ ticketId, memberId }) => {
                    axios
                      .post(
                        `${process.env.REACT_APP_API_BASE}/subscriptions/checkout/${ticketId}/${memberId}`
                      )
                      .then((response) => {
                        history.push(`/checkout/${response.data.sessionId}`);
                      })
                      .catch((err) => {
                        console.log(err);
                        alert(
                          "Ocorreu algum erro ao proceder para o pagamento. Por favor verifique seus dados e tente novamente!"
                        );
                      });
                  };

                  return (
                    <div>
                      <header>
                        <h3>{subscription.ticket.event.name}</h3>
                        <p>
                          De {strStart} a {strStop}
                        </p>
                      </header>
                      <div>
                        <p>
                          {subscription.ticket.description}<br/>
                          Valor: R${" "}
                          {parseFloat(subscription.ticket.price).toFixed(2)}<br/>
                          Status:{" "}
                          {subscription.status === 1
                            ? [ <span>Pago</span>,
                            <span>Confirmação de pagamento: {subscription.paymentId}</span> ]
                            : <span>Aguardando pagamento <button onClick={e => checkout({
                              ticketId: subscription.ticket.id,
                              memberId: subscription.member.id
                            })}>Realizar pagamento</button></span>
                            }<br/>
                            <pre>{subscription.paymentId}</pre>
                        </p>
                      </div>
                    </div>
                  );
                })}
            </section>
          </section>
        ) : (
          <div>
            <InputText
              name="email"
              type="email"
              label="Email"
              formikProps={formik}
              />
            <InputText
              name="birth"
              type="date"
              label="Confirme sua data de nascimento"
              formikProps={formik}
            />
            <button onClick={check}>Confirmar identidade</button>
          </div>
        )}
      </main>
      <Footer />
    </div>
  );
};
