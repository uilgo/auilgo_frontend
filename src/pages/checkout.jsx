import React, { useEffect } from "react";
import { loadStripe } from "@stripe/stripe-js";

const stripePromise = loadStripe("pk_test_E5FgfgLYeEm2RYumlY8U43JU00L93cvd8T");

export default (props) => {
  useEffect(async () => {
    console.log(props);
    const stripe = await stripePromise;
    const { error } = await stripe.redirectToCheckout({
      sessionId: props.match.params.sessionId,
    });
    if (error) {
      alert(error.message);
    }
  }, []);

  return <div>checkout</div>;
};
