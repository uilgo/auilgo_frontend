import React, { useContext } from 'react'
import { AppContext } from '../App'

export default () => {

  const context = useContext(AppContext);

  return [
    <div className="curtain" onClick={context.closeModal} />,
    <div className="modal">
    <button className="close" onClick={context.closeModal}>[ Fechar (X) ]</button> {context.ModalComponent}</div>
  ]
}
