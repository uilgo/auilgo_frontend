import React from 'react'

export default props => {
  return (
    <h2 className='step_title'>
      {props.step ? <div className='step_box'>{props.step}</div> : null}
      {props.children}
    </h2>
  )
}