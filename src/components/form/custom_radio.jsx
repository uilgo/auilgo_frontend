import React from 'react'

export default props => {

  const elem = props.elem;

  return (
    <div className={`custom_label_wrap ${props.className}`} key={elem.id}>
      <input 
        type="radio" 
        name={props.name} 
        id={`${props.name}_custom_label_${elem.id}`} 
        value={elem.id} 
        onChange={props.onChange}
      />
      <label for={`${props.name}_custom_label_${elem.id}`}>
        <div className="custom_label">
          <h3>{elem.description}</h3>
          <span className="price">R$ {parseFloat(elem.price).toFixed(2)}</span>
          <span className="price">Vagas: {elem.countNumber}</span>
        </div>
      </label>
    </div>
  )
}
