import React from "react";

export default ({ className, children, ...props }) => (
  <div className="uilgo_btn_wrap">
    <button className={`uilgo_btn ${className}`} {...props}>
      {children}
    </button>
  </div>
);
