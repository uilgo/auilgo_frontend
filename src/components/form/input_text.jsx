import React from "react";

export default ({
  name,
  type,
  onKeyUp,
  onChange,
  onBlur,
  className,
  value,
  placeholder,
  label,
  formikProps,
  ...props
}) => {
  return (
    <div className="input_wrap">
      <label for={name}>{label}</label>
      <input
        type={type || "text"}
        id={name}
        name={name}
        className={className}
        onChange={onChange}
        value={value}
        placeholder={placeholder}
        onChange={e => {
          onChange && onChange(e)
          formikProps.handleChange(e)
        }}
        onKeyUp={e => {
          onKeyUp && onKeyUp(e)
          formikProps.handleChange(e)
        }}
        onBlur={e => {
          onBlur && onBlur(e)
          formikProps.handleChange(e)
        }}
        value={formikProps.values[name]}
        {...formikProps.getFieldProps(name)}
        {...props}
      />
      {(formikProps.errors[name] && formikProps.touched[name]) && <span className="errors">{formikProps.errors[name]}</span>}
    </div>
  );
};
