import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Logo } from '../components/own_icons'

export default class HeaderMain extends Component {
  constructor(props){
    super(props)

    this.state = {
      navIsOpened: true
    }
  }

  componentDidMount(){
    if(window.innerWidth > 768){
      this.setState({navIsOpened : true})
    }else{
      this.setState({navIsOpened : false})
    }
  }

  toggleNav = () =>{
    this.state.navIsOpened ? this.setState({ navIsOpened : false }) : this.setState({ navIsOpened : true })
    console.log(this.state.navIsOpened)
  }

  render() {
    return (
      <>
      <header className="main container">
        <Logo className="logo"/>
        <p>Solução inteligente em inscrição de eventos</p>
        <button onClick={this.toggleNav} className={`just_mobile nav_bt ${this.state.navIsOpened ? "opened" : null}`}><i class="fas fa-bars"></i></button>
        <nav className={this.state.navIsOpened ? "opened" : "closed"}>
          <ul>
            <li><Link to="/">Eventos</Link></li>
            <li><Link to="/perfil">Meu Perfil</Link></li>
          </ul>
        </nav>        
      </header>
      <div className="page_title">
        <div className="container">
          <h1>{this.props.title}</h1>
          {/* <div className="search_input"><i className="fas fa-search"></i></div> */}
        </div>
      </div>
      </>
    )
  }
}

