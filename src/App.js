import React, { Component } from 'react';
// import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Router, Route, Switch } from 'react-router-dom';
import './sass/app.scss';
import SimplePage from './pages/simple_page';
import ModalBox from './components/modal_box';
import asyncComponent from './functions/async_component';
import history from './functions/history';

const AsyncEvento = asyncComponent(() => import("./pages/evento"));
const AsyncPerfil = asyncComponent(() => import("./pages/perfil"));
const AsyncCheckout = asyncComponent(() => import("./pages/checkout"));
const AsyncCheckoutSuccess = asyncComponent(() => import("./pages/checkout_success"));
const AsyncCheckoutCanceled = asyncComponent(() => import("./pages/checkout_canceled"));

export const AppContext = React.createContext();

class App extends Component {
  constructor(props){
    super(props)

    // State com todos os dados globais necessários para a aplicação
    this.state = {
      // Context state
      user: {
        id: null
      },
      member: {},
      setMember: member => this.setState({ member }),
      ModalComponent: null,
      changeModalComponent: component => this.setState({ ModalComponent : component}),
      modalIsVisible: false,
      openModal: () => this.setState({ modalIsVisible : true }),
      closeModal: () => this.setState({ modalIsVisible : false })
    };
  }

  render() {
    return (
    <Router history={history}>
      <AppContext.Provider value={this.state} >
      <AppContext.Consumer>
        { context =>  context.modalIsVisible ? <ModalBox/> : null }
      </AppContext.Consumer>
      <Switch>
        <Route exact path="/" component={SimplePage} />   
        <Route exact path="/evento/:id" component={AsyncEvento} />
        <Route exact path="/checkout/:sessionId" component={AsyncCheckout} />
        <Route exact path="/pagamento-efetuado" component={AsyncCheckoutSuccess} />
        <Route exact path="/pagamento-cancelado" component={AsyncCheckoutCanceled} />
        <Route exact path="/perfil" component={AsyncPerfil} />
      </Switch>
      </AppContext.Provider>
    </Router>
    );
  }
}

export default App;
